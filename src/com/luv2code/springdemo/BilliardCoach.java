package com.luv2code.springdemo;

public class BilliardCoach implements Coach {
	private FortuneService fortuneService;
	
	public BilliardCoach(FortuneService theFortuneService) {
		fortuneService = theFortuneService;
	}

	@Override
	public String getDailyWorkout() {
		// TODO Auto-generated method stub
		return "Make a warm up";
	}

	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}

	
	

	
	
	
}

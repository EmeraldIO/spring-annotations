package com.luv2code.springdemo;

import java.util.Random;

//import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RandomFortuneService implements FortuneService {

	//create an array of Strings 
	private String[] data = {
			"Beware of the wolf in sheep's clothing",
			"Diligence is the mother of good luck",
			"The journey is the reward"	
	};
	
	@Value("${foo.fortunes}")
	private String fortunesStack;
	
		
	//create a random number generator
	private Random myRandom = new Random();
//	@PostConstruct
	@Override
	public String getFortune() {
//		String[] data = fortunesStack.split(",");
		int index = myRandom.nextInt(data.length);
		
		String theFortune = data[index];

		
		return theFortune;
	}

}

package com.luv2code.springdemo;

import org.springframework.stereotype.Component;

@Component
public class SquashCoach implements Coach {

	@Override
	public String getDailyWorkout() {
		return "Run 1km and 45 push ups";
	}

	@Override
	public String getDailyFortune() {
		return null;
	}

}

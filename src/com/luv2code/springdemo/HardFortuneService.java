package com.luv2code.springdemo;

public class HardFortuneService implements FortuneService {

	@Override
	public String getFortune() {
		return "Push 50 balls";
	}

}
